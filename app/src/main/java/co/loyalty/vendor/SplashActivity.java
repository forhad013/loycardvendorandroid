package co.loyalty.vendor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SplashActivity extends AppCompatActivity {

    SharePref sharePref;
    boolean previousUser;
    Button startTour,skiptour;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        getActionBar().hide();
        setContentView(R.layout.activity_splash);

        sharePref = new SharePref(getApplicationContext());


        previousUser = sharePref.getshareprefdataBoolean(SharePref.PREVIOUS_USER);

        if(previousUser){

            startActivity(new Intent(SplashActivity.this,
                    VendorActivity.class));
            finish();

        }

        startTour = (Button) findViewById(R.id.start);
        skiptour = (Button) findViewById(R.id.skip);

     startTour.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             startActivity(new Intent(SplashActivity.this,
                     FirstTutorialActivity.class));
             finish();
         }
     });
        skiptour.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
             startActivity(new Intent(SplashActivity.this,
                     VendorActivity.class));
             finish();
         }
     });




    }
}
