package co.loyalty.vendor.data_models;

/**
 * Created by samclough on 23/05/17.
 */

public class Subscription {

    public Boolean subscribed;

    public Subscription() {}

    public Subscription(Boolean subscribed) {
        this.subscribed = subscribed;
    }


}
